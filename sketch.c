#include "Arduino.h"
#include "pins_arduino.h"

const uint8_t leds[] = { 18, 15, 2, 3, 17, 16 };

void setup() {
  IOCON_R_PIO1_0 = 0b00011000001;
  IOCON_R_PIO1_1 = 0b00011000001;
  IOCON_R_PIO1_2 = 0b00011000001;
  IOCON_SWDIO_PIO1_3 = 0b00011000001;
  for (int i = 0; i < 6; i++)
    pinMode(leds[i], OUTPUT);
}

int effect = 0;
int pos = 0;

const int SHORT = 80;

const uint8_t expo[] = {
  1, 1, 1, 2, 2, 2, 3, 3, 4, 5, 6, 7, 9, 10, 12, 15, 17, 21, 25, 30,
  36, 43, 51, 61, 73, 87, 104, 125, 149, 178, 213, 255
};

void loop() {
  switch (effect) {
    case 0:
      for (int i = 0; i < 4; i++) {
        for (int j = 0; j <= 6; j++) {
          digitalWrite(leds[j % 6], LOW);
          delay(SHORT);
          digitalWrite(leds[j % 6], HIGH);
        }
        for (int j = 5; j >= 1; j--) {
          digitalWrite(leds[j], LOW);
          delay(SHORT);
          digitalWrite(leds[j], HIGH);
        }
      }
      break;
    case 1:
      for (int i = 0; i < 10; i++) {
        for (int i = 0; i < 6; i++)
          digitalWrite(leds[i], LOW);
        delay(120);
        for (int i = 0; i < 6; i++)
          digitalWrite(leds[i], HIGH);
        delay(120);
      }
      break;
    case 2: {
      long step = millis();
      int bright = 0;
      for (int a = 0; a < 3; a++) {
        for (int i = 0; i < 32; i++) {
          step += 10;
          while (millis() < step) {
            int j = 0;
            for (; j <= expo[i]; j++) {
              digitalWrite(leds[2 - a], LOW);
              digitalWrite(leds[3 + a], LOW);
            }
            for (; j < 256; j++) {
              digitalWrite(leds[2 - a], HIGH);
              digitalWrite(leds[3 + a], HIGH);
            }
          }
        }
        digitalWrite(leds[2 - a], LOW);
        digitalWrite(leds[3 + a], LOW);
      }
      for (int a = 0; a < 3; a++) {
        for (int i = 31; i >= 0; i--) {
          step += 10;
          while (millis() < step) {
            int j = 0;
            for (; j <= expo[i]; j++) {
              digitalWrite(leds[2 - a], LOW);
              digitalWrite(leds[3 + a], LOW);
            }
            for (; j < 256; j++) {
              digitalWrite(leds[2 - a], HIGH);
              digitalWrite(leds[3 + a], HIGH);
            }
          }
        }
        digitalWrite(leds[2 - a], HIGH);
        digitalWrite(leds[3 + a], HIGH);
      }
      break;
    }
    case 3:
      for (int i = 0; i < 3; i++)
        digitalWrite(leds[i], LOW);
      for (int i = 0; i < 24; i++) {
        digitalWrite(leds[i % 6], HIGH);
        digitalWrite(leds[(i + 3) % 6], LOW);
        delay(100);
      }
      for (int i = 0; i < 6; i++)
        digitalWrite(leds[i], HIGH);
      break;
    case 4: {
      long step = millis();
      for (int a = 0; a < 2; a++) {
        for (int i = 0; i < 32; i++) {
          step += 20;
          while (millis() < step) {
            int j = 0;
            for (; j <= expo[i]; j++) {
              for (int i = 0; i < 6; i++)
                digitalWrite(leds[i], LOW);
            }
            for (; j < 256; j++) {
              for (int i = 0; i < 6; i++)
                digitalWrite(leds[i], HIGH);
            }
          }
        }
        for (int i = 31; i >= 0; i--) {
          step += 20;
          while (millis() < step) {
            int j = 0;
            for (; j <= expo[i]; j++) {
              for (int i = 0; i < 6; i++)
                digitalWrite(leds[i], LOW);
            }
            for (; j < 256; j++) {
              for (int i = 0; i < 6; i++)
                digitalWrite(leds[i], HIGH);
            }
          }
        }
      }
      for (int i = 0; i < 6; i++)
        digitalWrite(leds[i], HIGH);
      break;
    }
    default:
      effect = 0;
      return;
  }
  effect++;
}
